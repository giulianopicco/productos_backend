from products.models import Category, Product, Image
from rest_framework import serializers
from django.contrib.auth.models import User


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ['image']


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['id', 'name']


class ProductSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True, read_only=True)
    categories = CategorySerializer(many=True, read_only=True, required=False)

    class Meta:
        model = Product
        fields = ['id', 'name', 'state', 'images', 'categories']


class ProductSerializerAnonimousUser(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['id', 'name', 'state']
