from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views
from .views import CategoryViewSet, ProductViewSet
from users.views import UserViewSet, UserLogIn


router = DefaultRouter()

router.register('products', ProductViewSet, basename='products')
router.register('auth', UserViewSet, basename='users')
router.register('categories', CategoryViewSet, basename='categories')

urlpatterns = [
    path('', include(router.urls)),
    path('api-token-auth/', views.obtain_auth_token),
    path('login/', UserLogIn.as_view(), name='login'),
]
