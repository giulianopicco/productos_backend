from django.shortcuts import render

from products.models import Category, Product
from .serializers import CategorySerializer, ProductSerializer, ProductSerializerAnonimousUser
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from rest_framework.response import Response


class CategoryViewSet(ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ProductViewSet(ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = Product.objects.all()

    def get_serializer_class(self):
        if self.request.user.is_anonymous:
            return ProductSerializerAnonimousUser
        return ProductSerializer

    def create(self, request, *args, **kwargs):
        category_set = request.data.pop('category_set', None)
        request.data.pop('image', None)  # TODO: handle image upload
        if category_set:
            product = Product.objects.create(**request.data)
            product.categories.set(category_set)
            return Response(ProductSerializer(product).data)
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        category_set = request.data.pop('category_set', None)
        if category_set:
            product = self.get_object()
            product.categories.set(category_set)
        return super().update(request, *args, **kwargs)
