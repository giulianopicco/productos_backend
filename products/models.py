from django.db import models


class Product(models.Model):
    NEW = 0
    USED = 1

    STATES = (
        (NEW, 'New'),
        (USED, 'Used'),
    )

    name = models.CharField(max_length=100)
    state = models.IntegerField(choices=STATES, default=NEW)

    class Meta:
        verbose_name_plural = 'products'

    def __str__(self):
        return self.name


class Image (models.Model):
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to='products/')

    class Meta:
        verbose_name_plural = 'images'

    def __str__(self):
        return self.product.name


class Category (models.Model):
    name = models.CharField(max_length=100)
    products = models.ManyToManyField(Product, related_name='categories')

    class Meta:
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name
