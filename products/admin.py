from django.contrib import admin

from products.models import Category, Product, Image


class MembershipInline(admin.TabularInline):
    model = Category.products.through


class ProductAdmin(admin.ModelAdmin):
    inlines = [
        MembershipInline,
    ]


admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
admin.site.register(Image)
