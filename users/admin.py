from django.contrib import admin

from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin

class UserAdmin(AuthUserAdmin):
    actions = ['activate_user','deactivate_user']

    def activate_user(self, request, queryset):
        queryset.update(is_active=True)

    def deactivate_user(self, request, queryset):
        queryset.update(is_active=False)

    activate_user.short_description = "Activate selected users"
    deactivate_user.short_description = "Deactivate selected users"

admin.site.unregister(User)
admin.site.register(User, UserAdmin)


